﻿using DependencyObject.Contract.InterfaceUI;
using DependencyObject.Contract.Services;
using DependencyObject.ViewModel.Abstract;
using Microsoft.Toolkit.Mvvm.Input;
using System.Windows.Input;

namespace DependencyObject.ViewModel;

public class ChildWindowViewModel : NotifyPropertyChanged, IChildWindowViewModel
{
    public ChildWindowViewModel(IDataAccess dataAccess)
    {
        ClearTextDataText = new RelayCommand(() => TextDataText = string.Empty);
        SetTextDataText = new RelayCommand(() => TextDataText = dataAccess.GetData());
        this.dataAccess = dataAccess;
    }


    public string TextDataText
    {
        get => textDataText; set
        {
            textDataText = value;
            OnPropertyChanged(nameof(TextDataText));
        }
    }

    public ICommand ClearTextDataText { get; }
    public ICommand SetTextDataText { get; }


    private string textDataText = string.Empty;


    private readonly IDataAccess dataAccess;
}
