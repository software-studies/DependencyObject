﻿using DependencyObject.Contract.AbstractFactory;
using DependencyObject.Contract.InterfaceUI;
using DependencyObject.Contract.Services;
using DependencyObject.ViewModel.Abstract;
using DependencyObject.Window;
using Microsoft.Toolkit.Mvvm.Input;
using System.Windows.Input;

namespace DependencyObject.ViewModel;

public class MainWindowViewModel : NotifyPropertyChanged, IMainWindowViewModel
{
    public MainWindowViewModel(IDataAccess dataAccess, IAbstractFactory<IChildWindow> childWindow)
    {
        ClearTextDataText = new RelayCommand(() => 
        TextDataText = string.Empty);
        SetTextDataText = new RelayCommand(() => TextDataText = dataAccess.GetData());
        AddChildForm = new RelayCommand(() => 
        childWindow.Create().Show());
        this.dataAccess = dataAccess;
    }


    public string TextDataText
    {
        get => textDataText; set
        {
            textDataText = value;
            OnPropertyChanged(nameof(TextDataText));
        }
    }

    public ICommand ClearTextDataText { get; }
    public ICommand SetTextDataText { get; }
    public ICommand AddChildForm { get; }


    private string textDataText = string.Empty;


    private readonly IDataAccess dataAccess;
}
