﻿namespace DependencyObject.Contract.Services;

public interface IDataAccess
{
    string GetData();
}
