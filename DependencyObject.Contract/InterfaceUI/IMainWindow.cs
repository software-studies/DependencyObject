﻿namespace DependencyObject.Contract.InterfaceUI;

public interface IMainWindow
{
    bool IsLoaded { get; }
    void Show();
    bool? ShowDialog();
    void Close();
}
