﻿namespace DependencyObject.Contract.InterfaceUI;

public interface IChildWindow
{
    bool IsLoaded { get; }
    void Show();
    bool? ShowDialog();
    void Close();
}
