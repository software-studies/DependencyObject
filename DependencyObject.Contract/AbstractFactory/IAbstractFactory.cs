﻿namespace DependencyObject.Contract.AbstractFactory
{
    public interface IAbstractFactory<T>
    {
        T Create();
    }
}