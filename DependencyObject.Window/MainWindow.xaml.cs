﻿using DependencyObject.Contract.InterfaceUI;
using DependencyObject.Contract.Services;

namespace DependencyObject.Window;

public partial class MainWindow : System.Windows.Window, IMainWindow
{
    private readonly IDataAccess dataAccess;

    public MainWindow(IMainWindowViewModel mainWindowViewModel, IDataAccess dataAccess)
    {
        InitializeComponent();
        this.DataContext = mainWindowViewModel;
        this.dataAccess = dataAccess;
    }
}
