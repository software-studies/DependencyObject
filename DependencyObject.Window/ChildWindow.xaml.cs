﻿using DependencyObject.Contract.InterfaceUI;
using DependencyObject.Contract.Services;

namespace DependencyObject.Window;

public partial class ChildWindow : System.Windows.Window, IChildWindow
{
    private readonly IDataAccess dataAccess;

    public ChildWindow(IChildWindowViewModel mainWindowViewModel, IDataAccess dataAccess)
    {
        InitializeComponent();
        this.DataContext = mainWindowViewModel;
        this.dataAccess = dataAccess;
    }
}


