﻿using DependencyObject.Contract.AbstractFactory;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DependencyObject.Main.StartUpHelpers;

public static class AbstractFactoryExtensions
{
    public static void AddAbstractFactory<TInteface, TImlementation>(this IServiceCollection services)
        where TInteface : class
        where TImlementation : class, TInteface
    {
        services.AddTransient<TInteface, TImlementation>();
        services.AddSingleton<Func<TInteface>>(x => () => x.GetService<TInteface>()!);
        services.AddSingleton<IAbstractFactory<TInteface>, AbstractFactory<TInteface>>();
    }
}
