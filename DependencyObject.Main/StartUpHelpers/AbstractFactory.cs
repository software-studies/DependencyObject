﻿using DependencyObject.Contract.AbstractFactory;
using System;

namespace DependencyObject.Main.StartUpHelpers;

public class AbstractFactory<T> : IAbstractFactory<T>
{
    private readonly Func<T> factory;

    public AbstractFactory(Func<T> factory)
    {
        this.factory = factory;
    }

    public T Create() => factory();
}
