﻿using DependencyObject.Contract.InterfaceUI;
using DependencyObject.Contract.Services;
using DependencyObject.DataAccess;
using DependencyObject.ViewModel;
using DependencyObject.Window;
using DependencyObject.Main.StartUpHelpers;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Windows;

namespace DependencyObject.Main;

public partial class App : Application
{
    public static IHost? AppHost { get; private set; }

    public App()
    {
        AppHost = Host.CreateDefaultBuilder()
            .ConfigureServices((hostContext, services) =>
            {
                services.AddSingleton<IMainWindow, MainWindow>();
                services.AddTransient<IMainWindowViewModel, MainWindowViewModel>();

                services.AddAbstractFactory<IChildWindow, ChildWindow>();

                //services.AddTransient<IChildWindow, ChildWindow>();


                services.AddTransient<IChildWindowViewModel, ChildWindowViewModel>();

                services.AddTransient<IDataAccess, DependencyObject.DataAccess.DataAccess>();
            })
            .Build();
    }

    protected override async void OnStartup(StartupEventArgs e)
    {
        await AppHost!.StopAsync();
        var startupForm = AppHost.Services.GetRequiredService<IMainWindow>();
        startupForm.Show();

        base.OnStartup(e);
    }

    protected override async void OnExit(ExitEventArgs e)
    {
        await AppHost!.StopAsync();
        base.OnExit(e);
    }
}
